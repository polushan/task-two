package task.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAnalytic {
    private String userId;
    private BigDecimal totalSum;
    private Map<String, AnalyticInfo> analyticInfo;
}
