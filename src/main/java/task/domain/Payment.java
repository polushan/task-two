package task.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Payment {
    @Id
    private String ref;
    private String categoryId;
    private String userId;
    private String recipientId;
    private String desc;
    private Double amount;
}
