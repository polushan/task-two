package task.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import task.domain.UserAnalytic;
import task.service.AnalyticService;

import java.util.List;

@RestController
@RequestMapping("/analytic")
public class AnalyticController {
    private AnalyticService analyticService;

    @Autowired
    public AnalyticController(AnalyticService analyticService) {
        this.analyticService = analyticService;
    }

    @GetMapping("/{userId}")
    public ResponseEntity<UserAnalytic> getAnalytic(@PathVariable String userId) {
        UserAnalytic analyticForUser = analyticService.getAnalyticForUser(userId);
        return ResponseEntity.ok(analyticForUser);
    }

    @GetMapping
    public ResponseEntity<List<UserAnalytic>> getAllAnalytic() {
        List<UserAnalytic> analyticForUser = analyticService.getAllAnalytic();
        return ResponseEntity.ok(analyticForUser);
    }

    @GetMapping("/analytic/{userId}/stats")
    public ResponseEntity getStats(@PathVariable String userId) {
        UserAnalytic analyticForUser = analyticService.getAnalyticForUser(userId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/analytic/{userId}/templates")
    public ResponseEntity getTemplates(@PathVariable String userId) {
        UserAnalytic analyticForUser = analyticService.getAnalyticForUser(userId);
        return ResponseEntity.ok().build();
    }
}
