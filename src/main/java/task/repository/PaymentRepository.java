package task.repository;

import org.springframework.data.repository.CrudRepository;
import task.domain.Payment;

import java.util.List;

public interface PaymentRepository extends CrudRepository<Payment, String> {
    List<Payment> findAllByUserId(String userId);
}
