package task.service;

import org.springframework.stereotype.Service;
import task.domain.AnalyticInfo;
import task.domain.Payment;
import task.domain.UserAnalytic;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AnalyticGenerator {
    public UserAnalytic generateAnalytic(List<Payment> allUserPayments, String userId) {
        UserAnalytic userAnalytic = new UserAnalytic();
        userAnalytic.setUserId(userId);

        Map<String, AnalyticInfo> analytic = new HashMap<>();

        BigDecimal totalSum = allUserPayments.stream()
                .map(payment -> {
                    String categoryId = payment.getCategoryId();
                    BigDecimal amount = BigDecimal.valueOf(payment.getAmount());
                    AnalyticInfo analyticInfo = analytic.getOrDefault(categoryId,
                            new AnalyticInfo(amount, amount, BigDecimal.ZERO));
                    if (amount.compareTo(analyticInfo.getMin()) < 0) {
                        analyticInfo.setMin(amount);
                    }
                    if (amount.compareTo(analyticInfo.getMax()) > 0) {
                        analyticInfo.setMax(amount);
                    }
                    analyticInfo.setSum(analyticInfo.getSum().add(amount));
                    analytic.putIfAbsent(categoryId, analyticInfo);
                    return amount;
                })
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        userAnalytic.setAnalyticInfo(analytic);
//        userAnalytic.setTotalSum(totalSum.setScale(2, RoundingMode.HALF_EVEN));
        userAnalytic.setTotalSum(totalSum);

        return userAnalytic;
    }
}
