package task.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import task.domain.Payment;
import task.repository.PaymentRepository;

@Slf4j
@Service
public class KafkaService {
    private PaymentRepository paymentRepository;

    @Autowired
    public KafkaService(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    @KafkaListener(topics = "${kafka.topic}", groupId = "${kafka.groupId}",
            topicPartitions = @TopicPartition(topic = "${kafka.topic}",
                    partitionOffsets = {@PartitionOffset(partition = "0", initialOffset = "0")}))
    public void consume(@Payload Payment payment) {
        log.info("New raw payment: {}", payment);
        paymentRepository.save(payment);
    }
}
