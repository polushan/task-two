package task.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import task.domain.AnalyticInfo;
import task.domain.Payment;
import task.domain.UserAnalytic;
import task.repository.PaymentRepository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class AnalyticService {
    private PaymentRepository paymentRepository;
    private AnalyticGenerator analyticGenerator;

    public AnalyticService(PaymentRepository paymentRepository, AnalyticGenerator analyticGenerator) {
        this.paymentRepository = paymentRepository;
        this.analyticGenerator = analyticGenerator;
    }

    public UserAnalytic getAnalyticForUser(String userId) {
        List<Payment> allUserPayments = paymentRepository.findAllByUserId(userId);

        if (CollectionUtils.isEmpty(allUserPayments)) {
            throw new IllegalStateException("Not found payments for user: " + userId);
        }
        return analyticGenerator.generateAnalytic(allUserPayments, userId);
    }

    public List<UserAnalytic> getAllAnalytic() {
        Iterable<Payment> allUsersPayments = paymentRepository.findAll();
        Map<String, List<Payment>> paymentsByUserId = StreamSupport.stream(allUsersPayments.spliterator(), false)
                .collect(Collectors.groupingBy(Payment::getUserId));
        return paymentsByUserId.entrySet()
                .stream()
                .map(entry -> analyticGenerator.generateAnalytic(entry.getValue(), entry.getKey()))
        .collect(Collectors.toList());
    }
}
